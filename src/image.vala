namespace Spacelaunch{

    public int downloadImage(string url, string nom) {

        int ls_status = -1;

        if(!GLib.FileUtils.test(nom, GLib.FileTest.EXISTS)){
            string ls_stdout;
	        string ls_stderr;
	        Process.spawn_command_line_sync("wget "+url+" -O "+nom, out ls_stdout, out ls_stderr, out ls_status);
        }

        return ls_status;
    }
}
