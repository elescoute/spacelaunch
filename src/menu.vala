namespace Spacelaunch {
    [GtkTemplate (ui = "/io/gitlab/elescoute/spacelaunch/ui/menuTheme.ui")]
    public class Widgets.Menu : Gtk.Box {

        [GtkChild]
        public unowned Gtk.CheckButton follow;
        [GtkChild]
        public unowned Gtk.CheckButton light;
        [GtkChild]
        public unowned Gtk.CheckButton dark;

        public Window win {get; set;}

        public Menu (Window win) {
            Object (
                win: win
            );

            follow.notify["active"].connect(changeTheme);
            light.notify["active"].connect(changeTheme);
            dark.notify["active"].connect(changeTheme);
            changeTheme();
        }

        construct {
            settings.bind ("follow",
                follow, "active",
                DEFAULT
            );
            settings.bind ("light",
                light, "active",
                DEFAULT
            );
            settings.bind ("dark",
                dark, "active",
                DEFAULT
            );
        }

        public void changeTheme()
        {

            if(follow.get_active ()) Adw.StyleManager.get_default ().color_scheme = DEFAULT;
            if(light.get_active ()) Adw.StyleManager.get_default ().color_scheme = FORCE_LIGHT;
            if(dark.get_active ()) Adw.StyleManager.get_default ().color_scheme = FORCE_DARK;

        }
    }
}
