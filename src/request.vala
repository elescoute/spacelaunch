namespace Spacelaunch{

    public class Requete : Object {

        public Window win {get;set;}

        public Requete (Window win) {
            Object (
                win: win
            );
        }

        public string[] requestUpcomingLaunchNext(string[] listeExistante, bool[] listeFiltres) {
            if(dev) message("requestUpcomingLaunchNext");
            string[] list = listeExistante;
            while(list.length < win.nbLancement && win.offset < 100){
                string url = "";
                url = "https://spacelaunchnow.me/api/ll/2.2.0/launch/upcoming/?offset="+win.offset.to_string();
                string[] listeTemp = analyseListe(url);
                string erreurServeur = lectureCle("detail",listeTemp[1]);
                bool erreur = false;
                if(erreurServeur.length > 1){
                      if(lectureCle("detail",listeTemp[1]).substring(0,21)=="Request was throttled") erreur = true;
                }
                if(!erreur){
                    string[] listeTemp0 = filtreListe(listeTemp, listeFiltres);
                    if(listeTemp[0]!="null") for(int i=1;i<listeTemp0.length;i++) list+=listeTemp0[i];
                    win.offset+=10;
                }
                else{
                    //stderr.printf("Erreur serveur\n");
                }
            }
            return list;
        }

        private string[] analyseListe(string url) {
            if(dev) message("analyseListe");
            string[] list = {};
            string chaine = "";
            int longueurMessage = 0;

            string fichier = dossierSuivants + "/" + GLib.Checksum.compute_for_string (ChecksumType.MD5, url) + ".dat";

            if(GLib.FileUtils.test(fichier, GLib.FileTest.EXISTS)) {
                //SI LE FICHIER EXISTE
                if(dev) message("Fichier existe");

                var dis = FileStream.open(fichier, "r");
                chaine = dis.read_line();
                longueurMessage = chaine.length;
            }
            else if(!GLib.FileUtils.test(fichier, GLib.FileTest.EXISTS)) {
                //ÉCRITURE DU FICHIER S'IL N'EXISTE PAS
                if(dev) message("MAJ Base de données");
	            string ls_stderr;
	            int ls_status;
                Process.spawn_command_line_sync("curl "+url, out chaine, out ls_stderr, out ls_status);
                var dis = FileStream.open (fichier, "w");
                dis.write(chaine.data);
                longueurMessage = chaine.length;
            }


            if(longueurMessage!=0){
                //SÉPARATION BLOCS
                bool stop = false;
                int debut = 0;
                //RECHERCHE DÉBUT BLOCS
                for(int i = 12;i < chaine.length ; i++){
                    if(chaine.substring(i-11,11) == "\"results\":[" && !stop){
                        debut = i;
                        stop = true;
                    }
                }
                //DÉCOUPAGE DES BLOCS

                int acc = 0;
                int debutBloc = 0;
                int finBloc = 0;
                int nbBloc = 0;
                for(int i = debut; i < chaine.length ; i++){
                    if(chaine[i]=='{'){
                        if(acc==0) debutBloc = i;
                        acc++;
                    }
                    if(chaine[i]=='}'){
                        acc--;
                        if(acc==0){
                            finBloc = i;
                            list += chaine.substring(debutBloc,finBloc-debutBloc);
                            nbBloc++;
                        }
                    }
                }
            }
            else{
                list[0]="null";
            }

            return list;
        }



        private string[] filtreListe(string[] listeEntree, bool[] listeFiltres) {
            if(dev) message("filetreListe");
            //VÉRIFICATION FILTRES
            string[] listeSortie = {""};

            for(int i = 0; i < listeEntree.length-1; i++){
                bool filtreOK = false;
                string codePays = lectureCle("country_code", listeEntree[i]);

                if(codePays=="CHN"){if(listeFiltres[0]) filtreOK = true;}
                else if(codePays=="GUF"){if(listeFiltres[1]) filtreOK = true;}
                else if(codePays=="IND"){if(listeFiltres[2]) filtreOK = true;}
                else if(codePays=="JPN"){if(listeFiltres[3]) filtreOK = true;}
                else if(codePays=="KAZ"){if(listeFiltres[4]) filtreOK = true;}
                else if(codePays=="NZL"){if(listeFiltres[5]) filtreOK = true;}
                else if(codePays=="RUS"){if(listeFiltres[6]) filtreOK = true;}
                else if(codePays=="USA"){if(listeFiltres[7]) filtreOK = true;}
                else if(listeFiltres[8]) filtreOK = true;

                if(filtreOK && listeFiltres[9]){
                    string mission = lectureCle("name", listeEntree[i]);
                    if(mission.contains("Starlink")) filtreOK = false;
                }

                if(filtreOK) listeSortie += listeEntree[i];
            }

            return listeSortie;
        }

        public int analyseId(string id, string[] list) {
            if(dev) message("analyseId");
            int num = 0;

            for(int i=0;i<list.length-1;i++){
                if(lectureCle("id",list[i]) == id) num = i;
            }

            return num;
        }

        public string requestData(string url, string dossier) {
            if(dev) message("requestData");
            string chaine = "";
            bool boucle = true;
            //while(boucle){

                string fichier = dossier + "/" + GLib.Checksum.compute_for_string (ChecksumType.MD5, url) + ".dat";
                if(!GLib.FileUtils.test(fichier, GLib.FileTest.EXISTS)) {
	                string ls_stderr;
	                int ls_status;
                    Process.spawn_command_line_sync("curl "+url, out chaine, out ls_stderr, out ls_status);
                    var dis = FileStream.open (fichier, "w");
                    dis.write(chaine.data);
                }
                else {
                    var dis = FileStream.open(fichier, "r");
                    chaine = dis.read_line();
                }

                string erreurServeur = lectureCle("detail",chaine);
                if(erreurServeur.length > 1){
                    if(lectureCle("detail",chaine).substring(0,21)=="Request was throttled"){
                        if(dev) message("Erreur serveur. Trop de requêtes.");
                    }
                }
                /*        boucle = true;
                    }
                    else{
                        boucle = false;
                    }
                }
                else{
                    boucle = false;
                }*/
            //}
            return chaine;
        }

        public bool checkUpdate() {
            if(dev) message("checkUpdate");
            bool aJour = true;
            string url = "https://spacelaunchnow.me/api/ll/2.2.0/launch/upcoming/?offset=0";
            string fichier = dossierSuivants + "/" + GLib.Checksum.compute_for_string (ChecksumType.MD5, url) + ".dat";
            if(dev) message(fichier);

            if(GLib.FileUtils.test(fichier, GLib.FileTest.EXISTS)) {
                //VERIFICATION DU MD5
                string md5local;
                string md5serveur;
                string chaine;
                string ls_stderr;
	            int ls_status;
	            //local
                Process.spawn_command_line_sync("md5sum "+fichier, out md5local, out ls_stderr, out ls_status);
                md5local = md5local.substring(0,32);
                if(dev) message("md5 local : "+md5local);
                //serveur

                Process.spawn_command_line_sync("curl -sL "+url, out chaine, out ls_stderr, out ls_status);
                md5serveur = GLib.Checksum.compute_for_string (ChecksumType.MD5, chaine).substring(0,32);
                if(dev) message("md5 serveur :"+md5serveur);

                if(md5local!=md5serveur){
                    if(dev) message("Pas à jour");
                    aJour=false;
                    //EFFACEMENT DU DOSSIER LANCEMENTS
                    Dir dir = Dir.open (dossierSuivants, 0);
                    string? name = null;
                    while ((name = dir.read_name ()) != null) {
                        string path = Path.build_filename (dossierSuivants, name);
                        if (FileUtils.test (path, FileTest.IS_REGULAR)) {
                            FileUtils.remove(path);
                        }
                    }
                    //RÉÉCRITURE DU FICHIER OFFSET 0
                    var dis = FileStream.open (fichier, "w");
                    dis.write(chaine.data);
                }
                else{
                    if(dev) message("A jour");
                }


            }
            return aJour;
        }
    }
}

