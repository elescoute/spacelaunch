/* window.vala
 *
 * Copyright 2022 Emilien Lescoute
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Spacelaunch {

	[GtkTemplate (ui = "/io/gitlab/elescoute/spacelaunch/ui/window.ui")]
	public class Window : Adw.ApplicationWindow {

        private const GLib.ActionEntry[] ACTION_ENTRIES = {
            { "preferences", on_preferences_action },
        };
        //[GtkChild] public unowned Gtk.Label titre;
        //[GtkChild] public unowned Gtk.Label sousTitre;
        [GtkChild] public unowned Gtk.MenuButton boutonMenu;
        [GtkChild] public unowned Gtk.Stack fenetre;
        [GtkChild] public unowned Gtk.Revealer revealRetour;
        [GtkChild] public unowned Gtk.Revealer revealRafraichissement;
        [GtkChild] public unowned Gtk.Spinner spinner;
		[GtkChild] public unowned Gtk.Box boiteSpinner;
		[GtkChild] public unowned Adw.ToastOverlay toast_overlay;
        [GtkChild] public unowned Gtk.Button boutonRafraichissement;
        [GtkChild] public unowned Gtk.Button boutonRetour;
        [GtkChild] private unowned Adw.ViewStack stack;
        [GtkChild] private unowned Adw.ViewStackPage stackEvenements;
        [GtkChild] private unowned Adw.ViewStackPage stackVehicules;


        public Widgets.Menu menu;
        public VueNormale vueNormale;
        public VueCompacte vueCompacte;
        public VueLancement vueLancement;
        public Reglages reglages;
        public Requete requete;

        public Gtk.Picture[] listePicNormale;
        private Adw.Toast notif;
        public int offset;
        public int nbLancement;
        private string id;
        private string[] liste;
        private bool vueSimple;
        private bool[] listeFiltres;
        private bool rechargement;
        private bool notifAffichee;
        public bool blocageRafraichissement;
        private bool compact;
        private int largeurFenetre;
        private int hauteurFenetre;
        public WebKit.WebView vueStream;
        public WebKit.Settings settingsStream;
        public Gtk.ScrolledWindow vueVideo;

		public Window (Gtk.Application app) {

			Object (application: app);
            add_action_entries (ACTION_ENTRIES, this);

            vueSimple = true;
            rechargement = false;
            notif = new Adw.Toast("");

            //titre.set_text("Space Launch");
            //sousTitre.set_text(_("Upcoming launches"));

            //CRÉATION LISTE FILTRES

            if(!verifFiltres()[0]) reinitialisationFiltres();
            listeFiltres = creationListeFiltres();

            //DIMENSIONS FENETRE

            this.set_default_size(settings.get_int("largeur"), settings.get_int("hauteur"));

            largeurFenetre = get_allocated_width();
            hauteurFenetre = get_allocated_height();

            boiteSpinner.hide();

            //PREMIÈRE LECTURE BASE DE DONNÉES ET LANCEMENT COMPTE À REBOURS

            GLib.Timeout.add (0, () => {
                appelRafraichissement();
                if(settings.get_boolean("refresh")) {
                    if(!requete.checkUpdate()) appelRafraichissement();
                }
                return false;
            });
            GLib.Timeout.add (1000, vueNormale.majCAR);
            GLib.Timeout.add (1000, vueLancement.majCAR);

            //CONNEXIONS

            boutonRafraichissement.clicked.connect(() => {
			    if(!blocageRafraichissement) {
                    if(fenetre.get_visible_child()==vueNormale || fenetre.get_visible_child()==vueCompacte){
			            vueNormale.revealBoutonPlus.set_reveal_child(false);
			            vueCompacte.revealBoutonPlusCompact.set_reveal_child(false);
			            if(!requete.checkUpdate()) {
                            appelRafraichissement();
                            if(fenetre.get_visible_child()==vueCompacte) basculVueCompacte();//ON FORCE LA RECRÉATION DE LA PAGE COMPACTE
                        }
			            else{
                            affichageNotification(_("Already up to date..."));
                            GLib.Timeout.add(500, () =>{redimensionnement(true);return false;});//POUR FAIRE APPARAÎTRE LE BOUTON PLUS SI LA VUE LE PERMET
                        }
                    }
                    else if(fenetre.get_visible_child()==vueLancement){
                        if(dev) message(id);
                    }
			    }
			});


			boutonRetour.clicked.connect(() => {
			    fenetre.set_transition_type(SLIDE_RIGHT);
			    if(fenetre.get_visible_child()==vueVideo) {
                    basculVueLancement();
                    vueStream.try_close();
                }
			    else if(!compact || fenetre.get_visible_child()==vueCompacte) basculVueNormale();
			    else basculVueCompacte();
			    fenetre.set_transition_type(SLIDE_LEFT);
			});

			//notif.dismissed.connect(() => {notifAffichee = false;});

            notify.connect(() => {redimensionnement(false);});

            this.close_request.connect(() => {//ecritureFichierDim();return false;});
                settings.set_int("largeur", get_width());
                settings.set_int("hauteur", get_height());
                return false;
            });


            //AJOUT VUE WEB

            vueStream = new WebKit.WebView();
            settingsStream = new WebKit.Settings();
            settingsStream.set_enable_webaudio(true);
            vueStream.set_settings(settingsStream);
            vueVideo = new Gtk.ScrolledWindow ();
            var viewStream = new Gtk.Viewport (null,null);
            vueVideo.set_child(viewStream);
            viewStream.set_child(vueStream);
            fenetre.add_named(vueVideo,"vueVideo");

            // 1.3.0

            /*if(settings.get_int("onglet") == 0) stack.set_visible_child_name("lancements");
            if(settings.get_int("onglet") == 1) stack.set_visible_child_name("evenements");
            if(settings.get_int("onglet") == 2) stack.set_visible_child_name("vehicules");



            stack.notify.connect(() => {
                if(stack.get_visible_child_name()=="lancements" && settings.get_int("onglet")!=0) {
                    settings.set_int("onglet",0);
                }
                else if(stack.get_visible_child_name()=="evenements" && settings.get_int("onglet")!=1) {
                    settings.set_int("onglet",1);
                }
                else if(stack.get_visible_child_name()=="vehicules" && settings.get_int("onglet")!=2) {
                    settings.set_int("onglet",2);
                }
            });*/

            stack.set_visible_child_name("lancements");
            stackEvenements.set_visible(false);
            stackVehicules.set_visible(false);

		}

		construct {

		    //AJOUT PAGES
            requete = new Requete(this);
            vueNormale = new VueNormale(this);
            vueCompacte = new VueCompacte(this);
            vueLancement = new VueLancement(this, requete);

		    //MENU
            menu = new Widgets.Menu(this);
            var pop = (Gtk.PopoverMenu)boutonMenu.get_popover ();
            pop.add_child (menu, "theme");

            fenetre.add_named(vueNormale,"vueNormale");
            fenetre.add_named(vueCompacte,"vueCompacte");
            fenetre.add_named(vueLancement,"vueLancement");
            //fenetre.add_named(vueWeb,"vueWeb");
		}

        public void basculVueNormale() {

            if(rechargement) appelRafraichissement();
            fenetre.set_visible_child(vueNormale);
            revealRetour.set_reveal_child(false);
            revealRafraichissement.set_reveal_child(true);
            GLib.Timeout.add(500, () =>{vueNormale.revealBoutonPlus.set_reveal_child(false);return false;});
            GLib.Timeout.add(500, () =>{redimensionnement(true);return false;});//POUR FAIRE APPARAÎTRE LE BOUTON PLUS SI LA VUE LE PERMET
            compact = false;
            //sousTitre.set_text(_("Upcoming launches"));
		}

		public void basculVueCompacte() {

            if(rechargement) appelRafraichissement();

            //CRÉATION PAGE SI VIDE
            if(vueCompacte.pageVide() || rechargement) {
                affichageImageChargement();
                appelCreationCompact();
                masquageImageChargement();
            }

            fenetre.set_visible_child(vueCompacte);
            revealRafraichissement.set_reveal_child(true);
            revealRetour.set_reveal_child(true);
            GLib.Timeout.add(500, () =>{redimensionnement(true);return false;});//POUR FAIRE APPARAÎTRE LE BOUTON PLUS SI LA VUE LE PERMET
            compact = true;
            //sousTitre.set_text(_("Upcoming launches"));
		}

		public void basculVueLancement() {

		    fenetre.set_visible_child(vueLancement);
            revealRafraichissement.set_reveal_child(false);
            revealRetour.set_reveal_child(true);
		}

        private void on_preferences_action () {

            new Reglages (this);
        }

        public void chgtFiltre() {
            listeFiltres = creationListeFiltres();
            if(fenetre.get_visible_child() == vueNormale) appelRafraichissement();
            else {
                rechargement = true;
                if(fenetre.get_visible_child() == vueCompacte) basculVueCompacte();
            }
        }

		//RAFRAICHISSEMENT VUE NORMALE

		public void appelRafraichissement() {

            affichageImageChargement();
			affichageNotification(_("Loading database..."));
		    if(rechargement) rechargement = false;
		    var loop = new MainLoop();
		    rafraichissement.begin((obj,res) => {
		        try{rafraichissement.end(res);}
		        catch(ThreadError e){}
		        loop.quit();
		    });
		    loop.run();
		    masquageImageChargement();
		    GLib.Timeout.add(500, () =>{redimensionnement(true);return false;});//POUR FAIRE APPARAÎTRE LE BOUTON PLUS SI LA VUE LE PERMET
		}

		private async void rafraichissement() throws ThreadError {

		    SourceFunc callback = rafraichissement.callback;

		    ThreadFunc<bool> run = () => {
                liste = {};
		        vueNormale.effacementPage();
		        vueCompacte.effacementPage();
                vueNormale.revealBoutonPlus.set_reveal_child(false);
                vueCompacte.revealBoutonPlusCompact.set_reveal_child(false);
                string[] nouvelleListe = requete.requestUpcomingLaunchNext(liste, listeFiltres);
                liste = nouvelleListe;
                if(liste.length > 0){
                    vueNormale.disposition(liste);
                    redimensionnement(true);
                    masquageNotification();
                }
                else{//ERREUR INTERNET
                    masquageNotification();
                    affichageNotification(_("Check your internet connection!"));
                }


                Idle.add((owned) callback);
			    return true;

			};
			new Thread<bool>("rafraichissement",run);
			yield;

			if(liste.length > 0){
			     // PRÉVOIR UNE PAGE À AFFICHER SI LISTE.LENGTH = 0;
			}
		}

		//CRÉATION PAGE COMPACT //LISTE DÉJÀ CHARGÉE POUR LES 10 PREMIERS LANCEMENTS. CRÉATION EN PARALLÈLE POUR PERMETTRE D'AFFICHER L'ANIMATION

		public void appelCreationCompact() {

		    affichageNotification(_("Loading additionnal launches..."));
            if(rechargement) rechargement = false;
            blocageRafraichissement = true;
		    var loop = new MainLoop();
		    creationCompact.begin((obj,res) => {
		        try{creationCompact.end(res);}
		        catch(ThreadError e){}
		        loop.quit();
		    });
		    loop.run();
		    blocageRafraichissement = false;

		}

		private async void creationCompact() throws ThreadError {

		    SourceFunc callback = creationCompact.callback;

		    ThreadFunc<bool> run = () => {
		        nbLancement = 10;
		        string[] nouvelleListe = requete.requestUpcomingLaunchNext(liste, listeFiltres);
                liste = nouvelleListe;
                vueCompacte.disposition(liste);
                masquageNotification();
		        Idle.add((owned) callback);
			    return true;
			};
			new Thread<bool>("creationCompact",run);
			yield;
		}


		//RAFRAICHISSEMENT COMPACT

		public void appelRafraichissementCompact() {

		    affichageNotification(_("Loading additionnal launches..."));
            blocageRafraichissement = true;
		    var loop = new MainLoop();
		    rafraichissementCompact.begin((obj,res) => {
		        try{rafraichissementCompact.end(res);}
		        catch(ThreadError e){}
		        loop.quit();
		    });
		    loop.run();
		    blocageRafraichissement = false;
            GLib.Timeout.add(500, () => {vueCompacte.revealBoutonPlusCompact.set_reveal_child(false);return false;});

		}

		private async void rafraichissementCompact() throws ThreadError {

		    SourceFunc callback = rafraichissementCompact.callback;

		    ThreadFunc<bool> run = () => {
		        if(offset<100){
		            //if(nbLancement==5){
		            //    nbLancement = 10;
		            //}
		            //else{
		                nbLancement+=10;
		            //}
		            while(liste.length-1 < nbLancement && offset<100){
		                liste = requete.requestUpcomingLaunchNext(liste, listeFiltres);
		            }
                    vueCompacte.disposition(liste);
                    masquageNotification();
		        }
		        Idle.add((owned) callback);
			    return true;
			};
			new Thread<bool>("rafraichissementCompact",run);
			yield;
		}

        //AFFICHAGE LANCEMENT

		public void appelLancement(Gtk.ListBoxRow row) {

            if(!blocageRafraichissement){
                masquageNotification();
                affichageImageChargement();
			    affichageNotification(_("Loading launch..."));
		        int num = requete.analyseId(row.get_parent().get_name(),liste);
                id=lectureCle("id",liste[num]);
    		    var loop = new MainLoop();
		        clicLancement.begin(num, (obj,res) => {
    		        try{clicLancement.end(res);}
    		        catch(ThreadError e){}
    		        loop.quit();
    		    });
    		    loop.run();

    		    basculVueLancement();
                masquageImageChargement();
            }
		}

        private async void clicLancement(int num) throws ThreadError {

            SourceFunc callback = clicLancement.callback;

            ThreadFunc<bool> run = () => {
                vueLancement.disposition(liste, num);
                masquageNotification();
                Idle.add((owned) callback);
                return true;
            };
            new Thread<bool>("clicLancement",run);
			yield;
            redimensionnement(true);
        }

		private void redimensionnement(bool force) {

		    if(largeurFenetre != get_width() || hauteurFenetre != get_height() || force){
                largeurFenetre = get_width();
                hauteurFenetre = get_height();

                int largeur = get_width();
                if(liste.length > 0){
                    if((vueSimple || force) && fenetre.get_visible_child()==vueLancement){
                        int largeurImage = int.min(vueLancement.clamp.get_maximum_size(),largeurFenetre);
                        int hauteurImage = (int) largeurImage * 300 / 720;
                        vueLancement.imageLanceur.set_size_request(largeurImage,hauteurImage);
                    }
                }
                if(vueSimple && largeur> 720){
		            vueSimple = false;
		        }
		        else if(!vueSimple && largeur<= 720){
    		        vueSimple = true;
	    	    }

	    	    //AFFICHAGE DES BOUTON PLUS SI SCROLLEDWINDOWS SUFFISAMMENT GRANDES
	    	    if(fenetre.get_visible_child()==vueNormale){
	    	        if(vueNormale.clamp.get_height()!=0 && vueNormale.clamp.get_height() == vueNormale.scrollVueNormale.get_height()
	    	        && !vueNormale.revealBoutonPlus.get_reveal_child()){
	    	            vueNormale.revealBoutonPlus.set_reveal_child(true);
	    	        }
	    	    }
	    	    if(fenetre.get_visible_child()==vueCompacte){
	    	        if(vueCompacte.clamp.get_height()!=0 && vueCompacte.clamp.get_height() == vueCompacte.scrollVueCompacte.get_height()
	    	        && !vueCompacte.revealBoutonPlusCompact.get_reveal_child()){
	    	            vueCompacte.revealBoutonPlusCompact.set_reveal_child(true);
	    	        }
	    	    }
            }
        }

        private void affichageImageChargement() {

            blocageRafraichissement = true;
            stack.hide();
            spinner.start();
            boiteSpinner.show();
        }

        private void masquageImageChargement() {

		    stack.show();
            boiteSpinner.hide();
            spinner.stop();
            blocageRafraichissement = false;
		}

		private void affichageNotification(string texte) {

            notif.set_title(texte);
			toast_overlay.add_toast(notif);
			notifAffichee = true;
			notif.dismissed.connect(() => {notifAffichee = false;});
		}

		private void masquageNotification() {

		    if(notifAffichee) try{notif.dismiss();} finally{};
		    notifAffichee = false;
		}
	}
}
